import { gql } from "@apollo/client";
import jwt from "jsonwebtoken";
import client from "@/constants/serverside-apollo-client";


const GET_NEW_ACCESSTOKEN = gql`
    query Query($refreshToken: String!) {
        getAccessToken(refreshToken: $refreshToken)
    }`

async function authenticated(accessToken, refreshToken) {
	let valid = false;
	let newToken = accessToken;
	try {
		jwt.verify(accessToken, process.env.JWT_TOKEN_SECRET_KEY);
		console.log(accessToken)
		console.log('valid accessToken')
		valid = true;
	} catch {
		accessToken = '';
	}
	if (!valid) {
		try {
			jwt.verify(refreshToken, process.env.JWT_TOKEN_SECRET_KEY);
			const TIME = `${new Date().getMinutes()}:${new Date().getSeconds()}`
			console.log('querying for new accessToken ' +  TIME)
			const accessToken = await client.query({
				query: GET_NEW_ACCESSTOKEN, variables: {
					refreshToken: refreshToken
				}
			});
			newToken = accessToken.data.getAccessToken;
			console.log('new token------------------------------------')
			console.log(accessToken.data.getAccessToken);
			valid = true;
		} catch (err) {
			return { authenticated: false, accessToken: accessToken }
		}
	}

	if (valid) console.log('user should stay authenticated')
	else {
		console.log('user should not stay authenticated')
	}
	return { authenticated: valid, accessToken: newToken };
}


export default authenticated
