import { gql } from "@apollo/client";
import client from "@/constants/serverside-apollo-client";

const GET_USER = gql`
    query {
        getUser {
            username
            id
        }
    }
`

const GetCurrentUser = async () => {
    try {
        const userResponse = await client.query({ query: GET_USER });
        return {
            username: userResponse.data.getUser.username,
            id: userResponse.data.getUser.id,
            token: userResponse.data.getUser.token,
            authenticated: true
        }
    } catch {
        return {
            authenticated: false
        }
    }
};


export default GetCurrentUser
