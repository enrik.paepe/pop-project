import { useRouter } from "next/router";
import { gql, useMutation } from "@apollo/client";
import styles from '../styles/Login.module.css';


const LOGIN = gql`
    mutation SignUp($username: String!, $password: String!) {
        registerUser(username: $username, password: $password) {
            code
            user {
                username
                id
            }
        }
    }
`
const Register = () => {
	const router = useRouter();
	const [loginMutation, { loading, error }] = useMutation(LOGIN);

	const handleSubmit = async (event) => {
		event.preventDefault();

		const username = event.target.username.value;
		const password = event.target.password.value;


		const resp = await loginMutation({
			variables: {
				username,
				password
			}
		});
		console.log(resp)
		if (resp.data.registerUser.code === 201) {
			console.log(resp.data)
			console.log('should redirect')
			await router.push('/login')
		}

	};

	return (
		<div className={styles.pageContainer}>
			<form className={`${styles.container} ${styles.form}`} onSubmit={handleSubmit}>
				<label className={styles.label} htmlFor="username">Username</label>
				<input className={styles.input} type="text" name="username" id="username" required/>
				<label className={styles.label} htmlFor="password">Password</label>
				<input className={styles.input} type="password" name="password" id="password" required/>
				<button className={styles.button} type="submit">Submit</button>
			</form>
		</div>
	);
};

export default Register;
