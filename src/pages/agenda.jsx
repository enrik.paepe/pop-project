import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import session from "@/constants/serverside-session";
import { UNAUTHENTICATED_RESPONSE } from "@/constants/urls";
import authenticated from "@/hooks/useAuthenticatedRequest";
import Sidebar from "@/components/Sidebar";
import styles from '../styles/Agenda.module.css';
import GeneralBody from "@/components/GeneralBody";
import { gql } from "@apollo/client";
import client from "@/constants/serverside-apollo-client";
import AgendaBody from "@/components/Agenda";

const GET_ACTIVITIES = gql`
    query getActivities {
        getActivities {
			id
			description
			date
        }
    }
`

export async function getServerSideProps({ req, res }) {
	const response = await session.runAndReturn(async () => {
		session.set('accessToken', req.cookies.accessToken);
		const auth = await authenticated(req.cookies.accessToken, req.cookies.refreshToken);
		let activities = [];
		if(auth.authenticated){
			const activitiesRes = await client.query({ query: GET_ACTIVITIES });
			activities = activitiesRes.data.getActivities
		}
		return {
			user: auth,
			activities: activities
		}
	});
	if (!response.user.authenticated) {

		return UNAUTHENTICATED_RESPONSE;
	}
	req.cookies.accessToken = response.user.accessToken;
	return {
		props: {
			activities: response.activities
		}
	}
}

const Agenda = ({activities}) => {
	const router = useRouter();
	const days = getCurrentWeekWithActivities(activities);


	console.log(days)


	return (
		<div className={styles.container}>
			<AgendaBody days={days}></AgendaBody>
		</div>
	);
};


function getCurrentWeekWithActivities(activities) {
	const today = new Date();
	const currentDayOfWeek = today.getDay();

	const startDate = new Date(today);
	startDate.setDate(today.getDate() - currentDayOfWeek + 1);

	const weekWithActivities = [];
	for (let i = 0; i < 7; i++) {
		const currentDate = new Date(startDate.getTime() + i * 24 * 60 * 60 * 1000);
		const day = {
			date: currentDate,
			activities: []
		};
		activities.forEach(activity => {
			const activityDate = new Date(activity.date * 1);

			if (
				activityDate.getDate() === currentDate.getDate() &&
				activityDate.getMonth() === currentDate.getMonth() &&
				activityDate.getFullYear() === currentDate.getFullYear()
			) {
				day.activities.push(activity);
			}
		});

		weekWithActivities.push(day);
	}
	return weekWithActivities;
}

export default Agenda;
