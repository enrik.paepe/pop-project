import '@/styles/globals.css'
import { ApolloClient, ApolloProvider, InMemoryCache } from "@apollo/client";


export default function App({ Component, pageProps }) {
	const client = new ApolloClient({
		uri: 'http://localhost:4000',
		cache: new InMemoryCache(),
	});
	return (
		<>
          <ApolloProvider client={client}>
				  <Component {...pageProps} />
		  </ApolloProvider>
		</>
	)
}
