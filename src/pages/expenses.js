import GeneralBody from "@/components/GeneralBody";
import CreateRecordForm from "@/components/ActivityForm";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import session from "@/constants/serverside-session";
import authenticated from "@/hooks/useAuthenticatedRequest";
import { UNAUTHENTICATED_RESPONSE } from "@/constants/urls";
import AddExpenseForm from "@/components/AddExpenseForm";

export async function getServerSideProps({ req, res }) {
	const response = await session.runAndReturn(async () => {
		session.set('accessToken', req.cookies.accessToken);
		const auth = await authenticated(req.cookies.accessToken, req.cookies.refreshToken);
		return {
			user: auth,
		}
	});
	if (!response.user.authenticated) return UNAUTHENTICATED_RESPONSE;
	req.cookies.accessToken = response.user.accessToken;
	return {
		props: {
			accessToken: response.user.accessToken
		}
	}
}

export default function Page({ accessToken }) {
	const router = useRouter()


	const ADD_EXPENSE = gql`
        mutation AddExpense($date : String!, $description: String!, $amount: Int!) {
            addExpense(date: $date, description: $description, amount: $amount) {
                code
                response
            }
        }
	`

	const [add, { data, loading, error }] = useMutation(ADD_EXPENSE);
	const onAddExpense = async (data) => {
		const response = await add({
				variables: {
					date: data.date,
					description: data.description,
					amount: data.amount * 1

				},
				context: {
					headers: { authorization: accessToken }
				}

			},
		);
		if (response.data.addExpense.code === 201) router.push(`/`);

	}
	return (
		<GeneralBody page={'add expense'}>
			<AddExpenseForm onAddExpense={onAddExpense}/>
		</GeneralBody>
	)
}
