import GeneralBody from "@/components/GeneralBody";
import CreateRecordForm from "@/components/ActivityForm";
import { gql, useMutation } from "@apollo/client";
import { useRouter } from "next/router";
import session from "@/constants/serverside-session";
import authenticated from "@/hooks/useAuthenticatedRequest";
import { UNAUTHENTICATED_RESPONSE } from "@/constants/urls";

export async function getServerSideProps({ req, res }) {
	const response = await session.runAndReturn(async () => {
		session.set('accessToken', req.cookies.accessToken);
		const auth = await authenticated(req.cookies.accessToken, req.cookies.refreshToken);
		return {
			user: auth,
		}
	});
	if (!response.user.authenticated) return UNAUTHENTICATED_RESPONSE;
	req.cookies.accessToken = response.user.accessToken;
	return {
		props: {
			accessToken: response.user.accessToken
		}
	}
}

export default function Page({ accessToken }) {
	const router = useRouter()


	const ADD_ACTIVITY = gql`
        mutation CreateActivity($date : String!, $description: String!, $recurring: Boolean!, $duration: Int!, $exercise: Boolean!) {
            addActivity(date: $date, description: $description, recurring: $recurring, duration: $duration, exercise: $exercise) {
                code
                message
                activityId
            }
        }
	`

	const [add, { data, loading, error }] = useMutation(ADD_ACTIVITY);
	const createActivity = async (data) => {
		const response = await add({
				variables: {
					date: data.date,
					description: data.description,
					recurring: data.recurring === "true",
					duration: data.duration * 1,
					exercise: data.exercise === "true"
				},
				context: {
					headers: { authorization: accessToken }
				}

			},
		);
		if (response.data.addActivity.code === 201) router.push(`/activities/${response.data.addActivity.activityId}`);

	}
	return (
		<GeneralBody page={'add activity'}>
			<CreateRecordForm onCreateActivity={createActivity}/>
		</GeneralBody>
	)
}
