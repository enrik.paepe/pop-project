import { useRouter } from 'next/router'
import session from "@/constants/serverside-session";
import authenticated from "@/hooks/useAuthenticatedRequest";
import { UNAUTHENTICATED_RESPONSE } from "@/constants/urls";
import { gql, useMutation, useQuery } from "@apollo/client";
import QueryResult from "@/components/QueryResult";
import GeneralBody from "@/components/GeneralBody";
import SpecificActivity from "@/components/SpecificActivity";
import { useState } from "react";


const GET_ACTIVITY = gql`
    query GetActivity($id: Int!) {
        getActivity(id: $id ) {
            date
            description
            id
            duration
        }
    }
`

const DELETE_ACTIVITY = gql`
    mutation DeleteActivity($id : Int!) {
        deleteActivity(id: $id) {
            code
            response
        }
    }
`

export async function getServerSideProps({ req, res }) {
	const response = await session.runAndReturn(async () => {
		session.set('accessToken', req.cookies.accessToken);
		const auth = await authenticated(req.cookies.accessToken, req.cookies.refreshToken);
		return {
			user: auth,
		}
	});
	if (!response.user.authenticated) return UNAUTHENTICATED_RESPONSE;

	req.cookies.accessToken = response.user.accessToken;
	return {
		props: {
			accessToken: response.user.accessToken
		}
	}
}

export default function Page({ accessToken }) {
	const router = useRouter()

	const [deleteMessage, setDeleteMessage] = useState('')
	const { loading, error, data } = useQuery(GET_ACTIVITY, {
		variables: { id: router.query.id * 1 },
		context: {
			headers: { authorization: accessToken }
		}
	});

	const [delActivity, { dataDelete }] = useMutation(DELETE_ACTIVITY, {
		variables: {
			id: router.query.id * 1
		},
		context: {
			headers: { authorization: accessToken }
		}
	});

	const deleteActivity = async () => {
		const deleteResponse = await delActivity({ variables: data.getActivity.id })
		console.log(dataDelete)
		if(deleteResponse.data.deleteActivity.code === 200) router.push('/agenda');
		setDeleteMessage('Something went wrong');
	}

	function returnActivityBox() {
		if (error || !data?.getActivity) return <div>Activity not found</div>
		if (loading) return <div>Loading...</div>
		else return <SpecificActivity activity={data.getActivity} onDeleteEvent={deleteActivity}></SpecificActivity>
	}

	return <div>
		<QueryResult error={error} loading={loading} data={data}>
			<GeneralBody page={'activity'}>
				{returnActivityBox()}
			</GeneralBody>
			{deleteMessage}
		</QueryResult>
	</div>
}

