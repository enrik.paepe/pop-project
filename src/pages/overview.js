import session from "@/constants/serverside-session";
import authenticated from "@/hooks/useAuthenticatedRequest";
import { UNAUTHENTICATED_RESPONSE } from "@/constants/urls";
import Overview from "@/components/Overview";
import { gql, useQuery } from "@apollo/client";
import jwt from "jsonwebtoken";

const GET_OVERVIEW = gql`
    query Activities {
        getDayOverview {
            activities {
                date
                description
                duration
                id
            }
            expenses
        }
    }
`

export async function getServerSideProps({ req }) {
	const response = await session.runAndReturn(async () => {
		session.set('accessToken', req.cookies.accessToken);
		const auth = await authenticated(req.cookies.accessToken, req.cookies.refreshToken);
		return {
			user: auth,
		}
	});
	if (!response.user.authenticated) {
		return UNAUTHENTICATED_RESPONSE;
	}
	req.cookies.accessToken = response.user.accessToken;
	console.log(response.user.accessToken)
	return {
		props: {
			accessToken: response.user.accessToken
		}
	}
}

export default function OverviewPage({ accessToken }) {


	const res = GetOverview(accessToken)

	return (<Overview expenses={res.expenses} activities={res.activities}/>)
}

const GetOverview = (accessToken) => {
	const { loading, error, data } = useQuery(GET_OVERVIEW, {
		context: {
			headers: { authorization: accessToken }
		}
	});
	if (error) return { activities: [], expenses: 0 }
	if (loading) return { activities: [], expenses: 0 }
	return { activities: data.getDayOverview.activities, expenses: data.getDayOverview.expenses }
}
