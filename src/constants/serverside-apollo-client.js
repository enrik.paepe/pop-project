import { ApolloClient, ApolloLink, concat, HttpLink, InMemoryCache } from "@apollo/client";
import session from "@/constants/serverside-session";


const httpLink = new HttpLink({
	uri: process.env.API_URL,
	credentials: "same-origin",
});

const authMiddleware = new ApolloLink((operation, forward) => {
	const accessToken =  session.get('accessToken');

	operation.setContext(({ headers = {} }) => ({
		headers: {
			...headers,
			authorization: accessToken || null,

		}
	}));
	return forward(operation);
})

const client = new ApolloClient({
	cache: new InMemoryCache(),
	link: concat(authMiddleware, httpLink),
});

export default client;
