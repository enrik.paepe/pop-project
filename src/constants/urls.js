

export const API_URL = process.env.NEXT_PUBLIC_API_URL

export const UNAUTHENTICATED_RESPONSE = {
	redirect: {
		destination: '/login'
	}
}

