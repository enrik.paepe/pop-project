import { createNamespace } from "cls-hooked";

const session = createNamespace('auth');

export default session;
