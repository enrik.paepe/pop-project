import styles from '../styles/SpecificActivity.module.css';

export default function SpecificActivity({ activity, onDeleteEvent })  {
	return (
		<div className={styles.container}>
			<div className={styles.descriptionContainer}>
				<p>{activity.date}</p>
				<p>{activity.description}</p>
				<p>{activity.duration} minutes</p>
				<p>Type: {activity.type ? 'exercise' : 'other'} </p>
			</div>
			<button className={styles.deleteButton} onClick={onDeleteEvent}>Delete activity</button>
		</div>
	)
}
