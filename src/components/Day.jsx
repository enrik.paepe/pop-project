import styles from '../styles/Day.module.css'
export default function Day ({ day }, key) {
	const convertedTime = formatDate(day.date)

	return (

		<div className={styles.container} key={key}>
			{convertedTime}
			{day.activities.map(activity => {
				return <li key={activity.id}>
					{activity.description}@{formatTime(activity.date)}

				</li>
			})
			}
		</div>
	)
}

function formatDate(date) {
	const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
	const day = daysOfWeek[date.getDay()];
	const dayNumber = date.getDate();

	let daySuffix = 'th';
	if (dayNumber === 1 || dayNumber === 21 || dayNumber === 31) {
		daySuffix = 'st';
	} else if (dayNumber === 2 || dayNumber === 22) {
		daySuffix = 'nd';
	} else if (dayNumber === 3 || dayNumber === 23) {
		daySuffix = 'rd';
	}

	return `${day} ${dayNumber}${daySuffix}`;
}
const formatTime = (timestamp) => {
	const date = new Date(timestamp * 1);
	return `${date.getHours()}:${date.getMinutes()}`;
}
