import styles from '../styles/GeneralHeader.module.css'
export default function GeneralHeader ({title})  {
	return <div className={styles.container}>
		<h1 className={styles.h1}>{title}</h1>
	</div>
}
