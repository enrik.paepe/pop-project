import SidebarOption from "@/components/SidebarOption";
import styles from '../styles/Sidebar.module.css';

export default function Sidebar() {
	return (
		<div className={styles.sidebar}>
			<SidebarOption image={'home.png'} link={'overview'}></SidebarOption>
			<SidebarOption image={'agenda.png'} link={'agenda'}></SidebarOption>
			<SidebarOption image={'expenses.png'} link={'expenses'}></SidebarOption>
			<SidebarOption image={'logout.png'} link={'logout'}></SidebarOption>
		</div>
	)
}





