import Day from "@/components/Day";
import styles from '../styles/Calendar.module.css'
export default function Calendar({ days }) {
	return (<div className={styles.container}>
		{days.map((day,i) => {
			return <Day day={day} key={i}></Day>
		})}
	</div>)
}

