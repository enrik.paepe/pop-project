import GeneralBody from "@/components/GeneralBody";
import Link from "next/link";
import styles from '../styles/Overview.module.css'

export default function Overview({ expenses, activities }) {
	return (
		<GeneralBody page={'overview'}>
			<h3>Total expenses this month: €{expenses}</h3>
			<h3>Activities planned for today:  </h3>
			{activities.map((activity) => {
				return <div className={styles.activityContainer} key={activity.id}>
					{activity.description} @
					{new Date(activity.date * 1).getHours()}:{new Date(activity.date * 1).getMinutes()}
					<br></br>
					<Link className={styles.linkBtn} href={`/activities/${activity.id}`}>See here</Link>
				</div>
			})}
		</GeneralBody>
	)
}
