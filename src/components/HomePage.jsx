import React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import styles from '../styles/Homepage.module.css'

const HomePage = () => {
	return (
		<div className={styles.container}>
			<Head>
				<title>Lifetracker</title>
			</Head>

			<header className={styles.header}>
				<h1 className={styles.title}>Expense Tracker & Agenda</h1>
				<p className={styles.description}>Stay organized with your finances and schedule.</p>
			</header>

			<main className={styles.main}>
				<div className={styles.ctaContainer}>
					<Link href="/login" className={styles.ctaButton}>
						Sign in
					</Link>
					<Link href="/register">
						Sign up
					</Link>
				</div>
			</main>
		</div>
	);
};

export default HomePage;
