import React from 'react';
import styles from '../styles/SidebarOption.module.css';
import Link from "next/link";
import Image from "next/image";

export default function MenuItem({ link, image }) {
	return (
		<div className={styles.menuItem}>
			<Link href={`/${link}`}>
				<Image src={`/images/${image}`} alt={link} height={100} width={100}/>
			</Link>
		</div>
	);
};

