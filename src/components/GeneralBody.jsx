import GeneralHeader from "@/components/GeneralHeader";
import Sidebar from "@/components/Sidebar";
import styles from '../styles/GeneralBody.module.css'

export default function GeneralBody({ page, children }) {
	return (
		<div className={styles.container}>
			<Sidebar></Sidebar>
			<div>
				<GeneralHeader title={page}></GeneralHeader>
				<div>
					{children}
				</div>
			</div>

		</div>
	)
}
