import { useState } from 'react';

function CreateRecordForm({onCreateActivity}) {
	const [formValues, setFormValues] = useState({
		date: '',
		description: '',
		recurring: 'true',
		duration: '',
		exercise: 'true',
	});

	const handleSubmit = (e) => {
		e.preventDefault();
		if (
			formValues.date.trim() === '' ||
			formValues.description.trim() === '' ||
			formValues.duration.trim() === '' ||
			formValues.exercise.trim() === ''
		) {
			return;
		}

		console.log('Form submitted:', formValues);
		onCreateActivity(formValues)
	};

	const handleChange = (e) => {
		const { name, value } = e.target;
		setFormValues((prevValues) => ({
			...prevValues,
			[name]: value,
		}));
	};

	return (
		<div className="form-container">
			<h2>Create Record</h2>
			<form onSubmit={handleSubmit}>
				<div className="form-group">
					<label htmlFor="date">Date</label>
					<input
						type="datetime-local"
						id="date"
						name="date"
						value={formValues.date}
						onChange={handleChange}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="description">Description</label>
					<input
						type="text"
						id="description"
						name="description"
						value={formValues.description}
						onChange={handleChange}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="recurring">Recurring</label>
					<select
						id="recurring"
						name="recurring"
						value={formValues.recurring}
						onChange={handleChange}
					>
						<option value="true">Yes</option>
						<option value="false">No</option>
					</select>
				</div>
				<div className="form-group">
					<label htmlFor="duration">Duration</label>
					<input
						type="text"
						id="duration"
						name="duration"
						value={formValues.duration}
						onChange={handleChange}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="exercise">Exercise</label>
					<select
						id="exercise"
						name="exercise"
						value={formValues.exercise}
						onChange={handleChange}
					>
						<option value="true">Yes</option>
						<option value="false">No</option>
					</select>
				</div>
				<button type="submit">Create</button>
			</form>
		</div>
	);
}

export default CreateRecordForm;
