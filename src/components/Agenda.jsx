import GeneralBody from "@/components/GeneralBody";
import Calendar from "@/components/Calendar";
import Link from "next/link";

const AgendaBody = ({ days }) => {
	const buttonStyle = {
		display: 'inline-block',
		padding: '10px 20px',
		backgroundColor: '#3498db',
		textDecoration: 'none',
		borderRadius: '5px',
		border: 'none',
		cursor: 'pointer',
		transition: 'background-color 0.3s ease',
		fontFamily: 'Arial, sans-serif'
	};
	return (
		<div>
			<GeneralBody page={'agenda'}>
				<Calendar days={days}></Calendar>
				<br></br>
				<Link style={buttonStyle} href={'/activities/add'}>Add activity</Link>
			</GeneralBody>


		</div>

	)


}

export default AgendaBody
