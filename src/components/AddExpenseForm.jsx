import { useState } from 'react';

function AddExpenseForm({onAddExpense}) {
	const [formValues, setFormValues] = useState({
		date: '',
		description: '',
		amount: '0'
	});

	const handleSubmit = (e) => {
		e.preventDefault();

		if (
			formValues.date.trim() === '' ||
			formValues.description.trim() === '' ||
			formValues.amount.trim() === ''
		) {
			return;
		}
		onAddExpense(formValues)
	};

	const handleChange = (e) => {
		const { name, value } = e.target;
		setFormValues((prevValues) => ({
			...prevValues,
			[name]: value,
		}));
	};

	return (
		<div className="form-container">
			<h2>Create Record</h2>
			<form onSubmit={handleSubmit}>
				<div className="form-group">
					<label htmlFor="date">Date</label>
					<input
						type="datetime-local"
						id="date"
						name="date"
						value={formValues.date}
						onChange={handleChange}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="description">Description</label>
					<input
						type="text"
						id="description"
						name="description"
						value={formValues.description}
						onChange={handleChange}
						required
					/>
				</div>
				<div className="form-group">
					<label htmlFor="amount">Amount (€)</label>
					<input
						type="number"
						id="amount"
						name="amount"
						value={formValues.amount}
						onChange={handleChange}
						required
					/>
				</div>
				<button type="submit">Add expense</button>
			</form>
		</div>
	);
}

export default AddExpenseForm;
